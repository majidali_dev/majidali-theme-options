<?php
/*
Plugin Name: Majid Ali Theme Options
Plugin URI: http://presstigers.com
Description: A wonderful panel for theme options to manage theme features.
Author: Majid Ali
Version: 1.0.0
Author URI: http://presstigers.com
*/

if( !defined('MAJIDALI_OPT') ){
    define( 'MAJIDALI_OPT', 'redux_builder_majidali' );
}
require plugin_dir_path(__FILE__) . 'admin/admin-init.php';