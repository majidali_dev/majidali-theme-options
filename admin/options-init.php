<?php

/**
 * For full documentation, please visit: http://docs.reduxframework.com/
 * For a more extensive sample-config file, you may look at:
 * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
 */
if (!class_exists('Redux')) {
    return;
}

// This is your option name where all the Redux data is stored.
$opt_name = "redux_builder_majidali";

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */
$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    'opt_name' => MAJIDALI_OPT,
    'display_name' => 'Theme Options',
    'display_version' => '1.0.0',
    'page_slug' => 'pt_theme_options',
    'page_title' => 'Theme Options',
    'update_notice' => true,
    'admin_bar' => true,
    'menu_type' => 'submenu',
    'menu_title' => 'Theme Options',
    'page_parent' => 'themes.php',
    'page_parent_post_type' => 'your_post_type',
    'default_mark' => '',
    'google_api_key' => 'AIzaSyASF9A6Mig-__M9N3qJDpEr8JmxBk6t8V4',
    'class' => 'pt-theme-opt',
    'hints' => array(
        'icon_position' => 'left',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'light',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'duration' => '500',
                'event' => 'mouseleave unfocus',
            ),
        ),
    ),
    'output' => true,
    'output_tag' => true,
    'settings_api' => true,
    'cdn_check_time' => '1440',
    'compiler' => true,
    'page_permissions' => 'manage_options',
    'save_defaults' => true,
    'show_import_export' => true,
    'database' => 'options',
    'transient_time' => '3600',
    'network_sites' => true,
    'use_cdn' => false,
    'dev_mode' => false,
    'plugin_row_meta' => false,
    'admin_notices' > false,
    'footer_credit' => 'Options panel provided by <a href="http://presstigers.com" target="_blank">PressTigers</a>'
);



Redux::setArgs($opt_name, $args);

/*
 * ---> END ARGUMENTS
 */


/*
 *
 * ---> START SECTIONS
 *
 */

/*
 * General options section,
 */
Redux::setSection($opt_name, array(
    'title' => __('General', 'ptmajidali'),
    'id' => 'general',
    'desc' => __('General Options', 'ptmajidali'),
    'icon' => 'el el-website',
    'fields' => array(
        array(
            'id' => 'body_padding',
            'type' => 'spacing',
            'title' => __('Top/Bottom Space', 'ptmajidali'),
            'subtitle' => __('Set top and bottom space of website container', 'ptmajidali'),
            'units' => 'px',
            'mode' => 'padding',
            'left' => false,
            'right' => false,
            'output' => array('#body-inner'),
            'default'            => array(
                'padding-top'     => '20px', 
                'padding-bottom'  => '80px', 
                'units'          => 'px', 
            )
        )
    )
));

/*
 * Header options section,
 */
Redux::setSection($opt_name, array(
    'title' => __('Header', 'ptmajidali'),
    'id' => 'header',
    'desc' => __('Header logos', 'ptmajidali'),
    'icon' => 'el el-home',
    'fields' => array(
        array(
            'id' => 'site_logo',
            'type' => 'media',
            'url' => true,
            'title' => __('Upload Logo', 'ptmajidali'),
            'subtitle' => __('Uplaod logo to show in header', 'ptmajidali'),
            'default' => array(
                'url' => get_template_directory_uri() . '/assets/images/logo.png'
            ),
        ),
        array(
            'id' => 'retina_logo',
            'type' => 'media',
            'url' => true,
            'title' => __('Upload Retina Logo', 'ptmajidali'),
            'subtitle' => __('Uplaod logo to show in header', 'ptmajidali'),
            'default' => array(
                'url' => get_template_directory_uri() . '/assets/images/logo-retina.png'
            ),
        ),
    )
));

/*
 * Typography options section,
 * getting font sizes for body, and headings
 */
Redux::setSection($opt_name, array(
    'title' => __('Typography', 'ptmajidali'),
    'id' => 'basic',
    'desc' => __('Setup font-sizes for content of website', 'ptmajidali'),
    'icon' => 'el el-font',
    'fields' => array(
        array(
            'title' => 'Website Default Font',
            'subtitle' => 'Select default font for website',
            'id' => 'body_font',
            'type' => 'typography',
            'font-backup' => true,
            'all_styles' => true,
            'text-align' => false,
            'units' => 'px',
            'color' => false,
            'default' => array(
                'font-style' => '400',
                'font-family' => 'PT Sans',
                'google' => true,
                'font-size' => '15px',
                'line-height' => '22px'
            ),
            'output' => array('body,p,div,ul,li,button,input[type="button"],input[type="reset"],input[type="submit"],input[type="text"],input[type="email"],input[type="tel"],input[type="search"],select,textarea'),
        ),
        array(
            'title' => 'Headings Font H-1',
            'subtitle' => 'Select font for H-1 heading tag',
            'id' => 'heading_font_h1',
            'type' => 'typography',
            'font-backup' => true,
            'all_styles' => true,
            'text-align' => false,
            'units' => 'px',
            'color' => false,
            'default' => array(
                'font-style' => '400',
                'font-family' => 'Arvo',
                'google' => true,
                'font-size' => '36px',
                'line-height' => '44px'
            ),
            'output' => array('h1'),
        ),
        array(
            'title' => 'Headings Font H-2',
            'subtitle' => 'Select font for H-2 heading tag',
            'id' => 'heading_font_h2',
            'type' => 'typography',
            'font-backup' => true,
            'all_styles' => true,
            'text-align' => false,
            'units' => 'px',
            'color' => false,
            'default' => array(
                'font-style' => '400',
                'font-family' => 'Arvo',
                'google' => true,
                'font-size' => '26px',
                'line-height' => '34px'
            ),
            'output' => array('h2'),
        ),
        array(
            'title' => 'Headings Font H-3',
            'subtitle' => 'Select font for H-3 heading tag',
            'id' => 'heading_font_h3',
            'type' => 'typography',
            'font-backup' => true,
            'all_styles' => true,
            'text-align' => false,
            'units' => 'px',
            'color' => false,
            'default' => array(
                'font-style' => '400',
                'font-family' => 'Arvo',
                'google' => true,
                'font-size' => '20px',
                'line-height' => '26px'
            ),
            'output' => array('h3'),
        ),
    )
));

/*
 * Color options,
 * choose colors for content/text, anchor
 */
Redux::setSection($opt_name, array(
    'title' => __('Colors', 'ptmajidali'),
    'id' => 'colors',
    'desc' => __('Choose colors for website content.', 'ptmajidali'),
    'icon' => 'el el-tint',
    'fields' => array(
        array(
            'id' => 'body_color',
            'type' => 'color',
            'transparent' => false,
            'title' => __('Text Color', 'ptmajidali'),
            'subtitle' => __('Select text color', 'ptmajidali'),
            'default' => '#555555',
            'output' => array('body,p,ul,ol,li,div'),
        ),
        array(
            'id' => 'headings_color',
            'type' => 'color',
            'transparent' => false,
            'title' => __('Headings Color', 'ptmajidali'),
            'subtitle' => __('Select headings color', 'ptmajidali'),
            'default' => '#2f5c8f',
            'output' => array('h1,h2,h3,h4'),
        ),
        array(
            'id' => 'link_color',
            'type' => 'color',
            'transparent' => false,
            'title' => __('Link Color', 'ptmajidali'),
            'subtitle' => __('Select link color', 'ptmajidali'),
            'default' => '#58718c',
            'output' => array('a,a:active'),
        ),
    )
));

/*
 * Footer options,
 * getting text for footer copyright
 */
Redux::setSection($opt_name, array(
    'title' => __('Footer', 'ptmajidali'),
    'id' => 'footer',
    'desc' => __('Footer copyright and other content', 'ptmajidali'),
    'icon' => 'el el-th-large',
    'fields' => array(
        array(
            'id' => 'footer_copyright',
            'type' => 'editor',
            'title' => __('Copyright Message', 'ptmajidali'),
            'subtitle' => __('Enter message to show in footer for copyright', 'ptmajidali'),
            'default' => __('Wordpress Theme Developed by <a href="http://presstigers.com" target="_blank">Majid Ali</a>', 'ptmajidali')
        ),
    )
));

/*
* <--- END SECTIONS
*/
